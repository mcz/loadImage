import java.io.File; // C'est cette fonction JAVA qui permet d'ouvrir une boite de dialog pour ouvrir un doc depuis le disque
PImage imgImport; // on crée une variable image ou l'on stokera l'image importée
int marge; 


void setup()
{
  size( 640, 480 );
  marge = 25;
  stroke(255);
}

void draw()
{
  background(60);
  text("• Appuyer sur i pour importer une image", 20, 20);
  line(0, marge, width, marge);
  
  if ( imgImport != null ) // ici on dit que SI ma variable image n'est pas vide (NULL) alors...
  {
    image( imgImport, 0, 30); // je l'affiche
  }
}

void choisirUneImage( File f ) // fonction pour avoir le chemin de l'image à charger
{
  if( f.exists() )
  {
     imgImport = loadImage( f.getAbsolutePath() ); 
  }
}

void keyPressed()
{
  if(key == 'i')
  {
    selectInput( "Select an image", "choisirUneImage" ); // selectInput( titre de l'action, nomde la fonction)
  }
}
